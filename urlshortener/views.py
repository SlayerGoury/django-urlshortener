from django.http import HttpResponse
from django.shortcuts import redirect
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from .utils import make_new_shortener, get_redirect, get_hits
import json
from json import JSONDecodeError


@method_decorator(csrf_exempt, name='dispatch')
class CreateView(View):
    """
    Expects json dictionary:
    {"url": "https://valid.url"}

    Returns json dictionary:
    {
        "url": <resulting short url>,
        "password": <password for requesting hit counter>
    }
    """

    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
        except JSONDecodeError:
            return HttpResponse('can not parse your json', status=400)

        try:
            path, password = make_new_shortener(data['url'])
        except KeyError:
            return HttpResponse('url is required', status=400)

        if not path:
            return HttpResponse('url is invalid', status=400)

        result = {
            'url': request.build_absolute_uri(path),
            'password': password,
        }
        return HttpResponse(json.dumps(result))


class GoView(View):
    """
    This is where tracking and redirection happens
    """

    def get(self, request, *args, **kwargs):
        target = get_redirect(kwargs['short_url'], request.META['REMOTE_ADDR'], request.META.get('HTTP_USER_AGENT'))
        if target:
            return redirect(target, status=302)
        else:
            return HttpResponse('not found', status=404)


@method_decorator(csrf_exempt, name='dispatch')
class GetCounterView(View):
    """
    Expects json dictionary:
    {"password": <password>}
    Returns json dictionary:
    {"hits": <int>}
    """

    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
        except JSONDecodeError:
            return HttpResponse('can not parse your json', status=400)

        try:
            hits = get_hits(kwargs['short_url'], data['password'])
        except KeyError:
            return HttpResponse('password is required', status=400)

        if hits == -1:
            return HttpResponse('url or password is invalid', status=403)

        return HttpResponse(json.dumps({'hits': hits}))
