from django.conf import settings
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.contrib.auth.hashers import ScryptPasswordHasher
from django.urls import reverse
from django.utils.crypto import get_random_string
import base64, uuid
from .models import ShortUrl, UrlTracker

hasher = ScryptPasswordHasher()
validator = URLValidator()


def make_new_shortener(url:str) -> tuple:
    """
    Make new shortener object and returh it's public path
    """

    try:
        validator(url)
    except ValidationError:
        return (None, None)
    password = get_random_string(16)
    hash = hasher.encode(password, settings.SECRET_KEY)
    shortener = ShortUrl.objects.create( target=url, secret=hash )
    path = base64.urlsafe_b64encode(shortener.uuid.bytes).rstrip(b'=').decode('ascii')
    return (reverse('go', args=[path]), password)


def _get_shortener(short_url:str):
    try:
        shortener_uuid = str(uuid.UUID(bytes=base64.urlsafe_b64decode(short_url + '==')))
    except AttributeError:
        return None
    try:
        shortener = ShortUrl.objects.get( uuid=shortener_uuid )
    except ShortUrl.DoesNotExist:
        return None
    return shortener


def get_redirect(short_url:str, ip:str, ua=None) -> str:
    """
    Retreive shortener target from public path and also track the request
    """

    if (shortener := _get_shortener(short_url)):
        UrlTracker.objects.create( shorturl=shortener, ip=ip, ua=ua )
        return shortener.target
    else:
        return ''


def get_hits(short_url:str, password:str) -> int:
    """
    Retreive hit counter for the url
    """

    if (shortener := _get_shortener(short_url)):
        hash = hasher.encode(password, settings.SECRET_KEY)
        if hash == shortener.secret:
            return UrlTracker.objects.filter( shorturl=shortener ).count()
    return -1
