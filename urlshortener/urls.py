from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('create',                   CreateView.as_view(),      name='create'),
    path('s/<str:short_url>',        GoView.as_view(),          name='go'),
    path('counter/<str:short_url>',  GetCounterView.as_view(),  name='counter'),
]
