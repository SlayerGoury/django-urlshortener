from django.test import TestCase, RequestFactory, Client
from django.urls import reverse
from .views import *
import json

c = Client(REMOTE_ADDR='127.0.0.1')


class UrlshortenerTests(TestCase):

    def SetUp():
        pass

    def TearDown():
        pass

    def test_can_create_new_shortener(self):
        r = c.post(reverse('create'), '{"url": "http://localhost"}', content_type='application/json')
        self.assertEqual(r.status_code, 200)

    def test_can_get_redirect(self):
        r = c.post(reverse('create'), '{"url": "http://localhost"}', content_type='application/json')
        url = json.loads(r.content)['url']
        r = c.get(url)
        self.assertEqual(r.status_code, 302)
        self.assertEqual(r.url, 'http://localhost')

    def test_can_not_create_bad_shortener(self):
        r = c.post(reverse('create'), '{"url": "not a valid address"}', content_type='application/json')
        self.assertEqual(r.status_code, 400)
        r = c.post(reverse('create'), 'not valid json', content_type='application/json')
        self.assertEqual(r.status_code, 400)
        r = c.post(reverse('create'), '{"no url key": "in this json"}', content_type='application/json')
        self.assertEqual(r.status_code, 400)

    def test_can_get_stats(self):
        r = c.post(reverse('create'), '{"url": "http://localhost"}', content_type='application/json')
        url = json.loads(r.content)['url']
        password = json.loads(r.content)['password']
        for i in range(10):
            r = c.get(url)
        slug = url.split('/')[-1]
        r = c.post(reverse('counter', args=[slug]), json.dumps({'password': password}), content_type='application/json')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(json.loads(r.content)['hits'], 10)


    def test_can_get_stats_even_for_zero_hits(self):
        r = c.post(reverse('create'), '{"url": "http://localhost"}', content_type='application/json')
        url = json.loads(r.content)['url']
        password = json.loads(r.content)['password']
        slug = url.split('/')[-1]
        r = c.post(reverse('counter', args=[slug]), json.dumps({'password': password}), content_type='application/json')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(json.loads(r.content)['hits'], 0)


    def test_can_not_get_stats_without_authentication(self):
        r = c.post(reverse('create'), '{"url": "http://localhost"}', content_type='application/json')
        url = json.loads(r.content)['url']
        for i in range(10):
            r = c.get(url)
        slug = url.split('/')[-1]
        r = c.post(reverse('counter', args=[slug]), '{"password": "meow"}', content_type='application/json')
        self.assertEqual(r.status_code, 403)
        r = c.post(reverse('counter', args=[slug]), '{"not password": "not meow"}', content_type='application/json')
        self.assertEqual(r.status_code, 400)
        r = c.post(reverse('counter', args=[slug]), 'not even a json', content_type='application/json')
        self.assertEqual(r.status_code, 400)
