from django.db import models
import uuid


class ShortUrl(models.Model):
    uuid        = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    target      = models.CharField(max_length=4096)
    secret      = models.CharField(max_length=512)


class UrlTracker(models.Model):
    shorturl    = models.ForeignKey(ShortUrl, on_delete=models.CASCADE)
    ip          = models.CharField(max_length=96)
    ua          = models.TextField(blank=True, null=True)
    when        = models.DateTimeField(auto_now_add=True)
